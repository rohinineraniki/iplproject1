const csvMatchesPath = "src/data/matches.csv";
const csvDeliveriesPath = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

function highestPlayerOfMatchForSeason(matchesJson) {
  if (matchesJson === undefined || !Array.isArray(matchesJson)) {
    return {};
  } else {
    let highestPlayerOfMatchForSeasonResult = {};
    for (let eachMatch of matchesJson) {
      let seasonKey = eachMatch.season;
      let playerOfMatchKey = eachMatch.player_of_match;
      if (seasonKey in highestPlayerOfMatchForSeasonResult) {
        if (
          playerOfMatchKey in highestPlayerOfMatchForSeasonResult[seasonKey]
        ) {
          highestPlayerOfMatchForSeasonResult[seasonKey][playerOfMatchKey] += 1;
        } else {
          if (playerOfMatchKey !== "") {
            highestPlayerOfMatchForSeasonResult[seasonKey][
              playerOfMatchKey
            ] = 1;
          }
        }
      } else {
        highestPlayerOfMatchForSeasonResult[seasonKey] = {
          [playerOfMatchKey]: 1,
        };
      }
    }

    const awardsResult = Object.entries(
      highestPlayerOfMatchForSeasonResult
    ).reduce((highestPlayerEachSeason, [season, playerOfMatchWithCount]) => {
      const counts = Object.values(playerOfMatchWithCount);
      const highestCount = Math.max(...counts);

      highestPlayerEachSeason[season] = Object.entries(playerOfMatchWithCount)
        .filter(([player, count]) => {
          return count === highestCount;
        })
        .map(([player, count]) => {
          return { player: player, numberOfTimes: count };
        });
      return highestPlayerEachSeason;
    }, {});
    return awardsResult;
  }
}
csvtojson()
  .fromFile(csvDeliveriesPath)
  .then((deliveriesJson) => {
    csvtojson()
      .fromFile(csvMatchesPath)
      .then((matchesJson) => {
        const highestPlayerOfMatchForSeasonResult =
          highestPlayerOfMatchForSeason(matchesJson, deliveriesJson);
        fs.writeFileSync(
          "src/public/output/6highestPlayerOfMatchForSeason.json",
          JSON.stringify(highestPlayerOfMatchForSeasonResult),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
