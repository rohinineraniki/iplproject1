const csvMatchesPath = "src/data/matches.csv";
const csvDeliveriesPath = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

function batsManStrikeRatePerSeason(matchesJson, deliveriesJson) {
  if (
    matchesJson === undefined ||
    deliveriesJson === undefined ||
    !Array.isArray(matchesJson) ||
    !Array.isArray(deliveriesJson)
  ) {
    return {};
  } else {
    let matchedIds = {};
    for (let eachMatch of matchesJson) {
      matchedIds[eachMatch.id] = eachMatch.season;
    }

    let batsmanRunDetailsPerYear = deliveriesJson.reduce(
      (batsmanRunDetailsPerYear, eachMatch) => {
        const year = matchedIds[eachMatch.match_id];
        batsmanRunDetailsPerYear[eachMatch.batsman] ??
          (batsmanRunDetailsPerYear[eachMatch.batsman] = {});
        batsmanRunDetailsPerYear[eachMatch.batsman][year] ??
          (batsmanRunDetailsPerYear[eachMatch.batsman][year] = {
            total_runs: 0,
            total_balls: 0,
          });
        batsmanRunDetailsPerYear[eachMatch.batsman][year]["total_runs"] +=
          isNaN(eachMatch.total_runs) ? 0 : Number(eachMatch.total_runs);
        batsmanRunDetailsPerYear[eachMatch.batsman][year]["total_balls"]++;

        return batsmanRunDetailsPerYear;
      },
      {}
    );

    const strikeRateOfBatsmanEachSeason = Object.keys(
      batsmanRunDetailsPerYear
    ).reduce((batsmanRunDetailsPerYear, eachPlayer) => {
      Object.keys(batsmanRunDetailsPerYear[eachPlayer]).reduce(
        (batsmanRunDetailsPerYear, year) => {
          const totalRuns =
            batsmanRunDetailsPerYear[eachPlayer][year]["total_runs"];
          const totalBalls =
            batsmanRunDetailsPerYear[eachPlayer][year]["total_balls"];

          batsmanRunDetailsPerYear[eachPlayer][year] = Number(
            ((totalRuns / totalBalls) * 100).toFixed(2)
          );

          return batsmanRunDetailsPerYear;
        },
        batsmanRunDetailsPerYear
      );

      return batsmanRunDetailsPerYear;
    }, batsmanRunDetailsPerYear);
    return strikeRateOfBatsmanEachSeason;
  }
}
csvtojson()
  .fromFile(csvDeliveriesPath)
  .then((deliveriesJson) => {
    csvtojson()
      .fromFile(csvMatchesPath)
      .then((matchesJson) => {
        const batsManStrikeRatePerSeasonResult = batsManStrikeRatePerSeason(
          matchesJson,
          deliveriesJson
        );
        fs.writeFileSync(
          "src/public/output/7batsManStrikeRatePerSeason.json",
          JSON.stringify(batsManStrikeRatePerSeasonResult),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
