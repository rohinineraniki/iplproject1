const csvMatchesPath = "src/data/matches.csv";
const csvDeliveriesPath = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

let matchesPerYearObject = {};

function matchesPerYear(matchesJson) {
  if (matchesJson === undefined || !Array.isArray(matchesJson)) {
    return {};
  } else {
    for (let eachMatch of matchesJson) {
      if (eachMatch.season in matchesPerYearObject) {
        matchesPerYearObject[eachMatch.season] += 1;
      } else {
        matchesPerYearObject[eachMatch.season] = 1;
      }
    }
    return matchesPerYearObject;
  }
}
csvtojson()
  .fromFile(csvDeliveriesPath)
  .then((deliveriesJson) => {
    csvtojson()
      .fromFile(csvMatchesPath)
      .then((matchesJson) => {
        const matchesPerYearResult = matchesPerYear(matchesJson);
        fs.writeFileSync(
          "src/public/output/1matchesPerYear.json",
          JSON.stringify(matchesPerYearResult),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });

module.exports = matchesPerYear;
