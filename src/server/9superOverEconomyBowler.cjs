const csvMatchesPath = "src/data/matches.csv";
const csvDeliveriesPath = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const { compareAsc } = require("date-fns");
const fs = require("fs");

function superOverEconomyBowler(deliveriesJson) {
  if (deliveriesJson === undefined || !Array.isArray(deliveriesJson)) {
    return {};
  } else {
    let superOverObject = {};
    for (let eachMatch of deliveriesJson) {
      if (eachMatch.is_super_over === "1") {
        if (eachMatch.bowler in superOverObject) {
          superOverObject[eachMatch.bowler].totalRuns += parseInt(
            eachMatch.total_runs
          );
          superOverObject[eachMatch.bowler].totalBalls += 1;
          superOverObject[eachMatch.bowler].totalByeRuns += parseInt(
            eachMatch.bye_runs
          );
          superOverObject[eachMatch.bowler].totalLegbyeRuns += parseInt(
            eachMatch.legbye_runs
          );
        } else {
          superOverObject[eachMatch.bowler] = {
            totalRuns: parseInt(eachMatch.total_runs),
            totalBalls: 1,
            totalByeRuns: parseInt(eachMatch.bye_runs),
            totalLegbyeRuns: parseInt(eachMatch.legbye_runs),
          };
        }
      }
    }
    console.log(superOverObject);
    for (let eachBowler in superOverObject) {
      totalOvers =
        parseInt(superOverObject[eachBowler].totalBalls / 6) +
        (superOverObject[eachBowler].totalBalls % 6) / 6;
      totalRunsForEconomy =
        superOverObject[eachBowler].totalRuns -
        (superOverObject[eachBowler].totalByeRuns +
          superOverObject[eachBowler].totalLegbyeRuns);
      economyRate = totalRunsForEconomy / totalOvers;
      superOverObject[eachBowler]["totalOvers"] = totalOvers;
      superOverObject[eachBowler]["economyRate"] = economyRate;
    }
    const bowlerSortedByEconomicRate = Object.entries(superOverObject).sort(
      (
        [playerA, { economyRate: playerAEconomy }],
        [playerB, { economyRate: playerBEconomy }]
      ) => {
        return playerAEconomy > playerBEconomy ? 1 : -1;
      }
    );

    let superOverEconomyRate = bowlerSortedByEconomicRate.slice(0, 1);
    return superOverEconomyRate;
  }
}
csvtojson()
  .fromFile(csvDeliveriesPath)
  .then((deliveriesJson) => {
    csvtojson()
      .fromFile(csvMatchesPath)
      .then((matchesJson) => {
        const superOverEconomyBowlerResult =
          superOverEconomyBowler(deliveriesJson);
        fs.writeFileSync(
          "src/public/output/9superOverEconomyBowler.json",
          JSON.stringify(superOverEconomyBowlerResult),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
