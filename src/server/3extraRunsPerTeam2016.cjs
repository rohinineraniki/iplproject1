const csvMatchesPath = "src/data/matches.csv";
const csvDeliveriesPath = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

function extraRunsPerTeam2016(matchesJson, deliveriesJson, year) {
  if (
    matchesJson === undefined ||
    deliveriesJson === undefined ||
    !Array.isArray(matchesJson) ||
    !Array.isArray(deliveriesJson)
  ) {
    return {};
  } else {
    let matchIdArray = [];
    let extraRunsPerTeam2016Result = {};
    for (let eachMatch of matchesJson) {
      if (parseInt(eachMatch.season) === year) {
        matchIdArray.push(eachMatch.id);
      }
    }
    for (let matchId of matchIdArray) {
      for (let eachMatch of deliveriesJson) {
        if (parseInt(matchId) === parseInt(eachMatch.match_id)) {
          if (eachMatch.bowling_team in extraRunsPerTeam2016Result) {
            extraRunsPerTeam2016Result[eachMatch.bowling_team] += parseInt(
              eachMatch.extra_runs
            );
          } else {
            extraRunsPerTeam2016Result[eachMatch.bowling_team] = parseInt(
              eachMatch.extra_runs
            );
          }
        }
      }
    }
    return extraRunsPerTeam2016Result;
  }
}
csvtojson()
  .fromFile(csvDeliveriesPath)
  .then((deliveriesJson) => {
    csvtojson()
      .fromFile(csvMatchesPath)
      .then((matchesJson) => {
        const extraRunsPerTeam2016Result = extraRunsPerTeam2016(
          matchesJson,
          deliveriesJson,
          2016
        );
        fs.writeFileSync(
          "src/public/output/3extraRunsPerTeam.json",
          JSON.stringify(extraRunsPerTeam2016Result),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
