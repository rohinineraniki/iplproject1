const csvMatchesPath = "src/data/matches.csv";
const csvDeliveriesPath = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

let matchesWonPerTeamPerYearObject = {};

function matchesWonPerTeamPerYear(matchesJson) {
  if (matchesJson === undefined || !Array.isArray(matchesJson)) {
    return {};
  } else {
    for (let eachMatch of matchesJson) {
      let winnerKey = eachMatch.winner;
      let seasonKey = eachMatch.season;
      if (winnerKey in matchesWonPerTeamPerYearObject) {
        if (seasonKey in matchesWonPerTeamPerYearObject[winnerKey]) {
          matchesWonPerTeamPerYearObject[winnerKey][seasonKey] += 1;
        } else {
          matchesWonPerTeamPerYearObject[winnerKey][seasonKey] = 1;
        }
      } else {
        matchesWonPerTeamPerYearObject[winnerKey] = { [seasonKey]: 1 };
      }
    }
    return matchesWonPerTeamPerYearObject;
  }
}
csvtojson()
  .fromFile(csvDeliveriesPath)
  .then((deliveriesJson) => {
    csvtojson()
      .fromFile(csvMatchesPath)
      .then((matchesJson) => {
        const matchesWonPerTeamPerYearResult =
          matchesWonPerTeamPerYear(matchesJson);
        fs.writeFileSync(
          "src/public/output/2matchesWonPerTeamPerYear.json",
          JSON.stringify(matchesWonPerTeamPerYearResult),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
