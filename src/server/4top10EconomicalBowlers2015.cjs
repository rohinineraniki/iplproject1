const csvMatchesPath = "src/data/matches.csv";
const csvDeliveriesPath = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const { compareAsc } = require("date-fns");
const fs = require("fs");

function top10EconomicalBowlers2015(matchesJson, deliveriesJson, year) {
  if (
    matchesJson === undefined ||
    deliveriesJson === undefined ||
    !Array.isArray(matchesJson) ||
    !Array.isArray(deliveriesJson)
  ) {
    return {};
  } else {
    let matchIdArray = [];
    let bowlerTotalRuns = {};
    for (let eachMatch of matchesJson) {
      if (parseInt(eachMatch.season) === year) {
        matchIdArray.push(eachMatch.id);
      }
    }

    for (let eachMatch of deliveriesJson) {
      for (let matchId of matchIdArray) {
        if (parseInt(eachMatch.match_id) === parseInt(matchId)) {
          if (eachMatch.bowler in bowlerTotalRuns) {
            bowlerTotalRuns[eachMatch.bowler].totalRuns += parseInt(
              eachMatch.total_runs
            );
            bowlerTotalRuns[eachMatch.bowler].totalBalls += 1;
            bowlerTotalRuns[eachMatch.bowler].totalByeRuns += parseInt(
              eachMatch.bye_runs
            );
            bowlerTotalRuns[eachMatch.bowler].totalLegbyeRuns += parseInt(
              eachMatch.legbye_runs
            );
            bowlerTotalRuns[eachMatch.bowler].totalPenaltyRuns += parseInt(
              eachMatch.penalty_runs
            );
          } else {
            bowlerTotalRuns[eachMatch.bowler] = {
              totalRuns: parseInt(eachMatch.total_runs),
              totalBalls: 1,
              totalByeRuns: parseInt(eachMatch.bye_runs),
              totalLegbyeRuns: parseInt(eachMatch.legbye_runs),
              totalPenaltyRuns: parseInt(eachMatch.penalty_runs),
            };
          }
        }
      }
    }
    console.log(bowlerTotalRuns);
    for (let eachBowler in bowlerTotalRuns) {
      totalOvers =
        parseInt(bowlerTotalRuns[eachBowler].totalBalls / 6) +
        (bowlerTotalRuns[eachBowler].totalBalls % 6) / 6;
      totalRunsForEconomy =
        bowlerTotalRuns[eachBowler].totalRuns -
        (bowlerTotalRuns[eachBowler].totalByeRuns +
          bowlerTotalRuns[eachBowler].totalLegbyeRuns);
      economyRate = totalRunsForEconomy / totalOvers;
      bowlerTotalRuns[eachBowler]["totalOvers"] = totalOvers;
      bowlerTotalRuns[eachBowler]["economyRate"] = economyRate;
    }
    const bowlerSortedByEconomicRate = Object.entries(bowlerTotalRuns).sort(
      (
        [playerA, { economyRate: playerAEconomy }],
        [playerB, { economyRate: playerBEconomy }]
      ) => {
        return playerAEconomy > playerBEconomy ? 1 : -1;
      }
    );

    let bowlerSortedByEconomicRateTop10 = bowlerSortedByEconomicRate.slice(
      0,
      10
    );
    return bowlerSortedByEconomicRateTop10;
  }
}
csvtojson()
  .fromFile(csvDeliveriesPath)
  .then((deliveriesJson) => {
    csvtojson()
      .fromFile(csvMatchesPath)
      .then((matchesJson) => {
        const top10EconomicalBowlers2015Result = top10EconomicalBowlers2015(
          matchesJson,
          deliveriesJson,
          2015
        );
        fs.writeFileSync(
          "src/public/output/4top10EconomicalBowlers2015.json",
          JSON.stringify(top10EconomicalBowlers2015Result),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
