const csvMatchesPath = "src/data/matches.csv";
const csvDeliveriesPath = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

function playerDismissed(deliveriesJson) {
  if (deliveriesJson === undefined || !Array.isArray(deliveriesJson)) {
    return {};
  } else {
    let dismissedPlayers = {};

    for (let eachMatch of deliveriesJson) {
      if (eachMatch.player_dismissed !== "") {
        if (eachMatch.player_dismissed in dismissedPlayers) {
          if (eachMatch.dismissal_kind !== "run out") {
            if (
              eachMatch.bowler in dismissedPlayers[eachMatch.player_dismissed]
            ) {
              dismissedPlayers[eachMatch.player_dismissed][
                eachMatch.bowler
              ] += 1;
            } else {
              dismissedPlayers[eachMatch.player_dismissed][
                eachMatch.bowler
              ] = 1;
            }
          } else {
            if (
              eachMatch.fielder in dismissedPlayers[eachMatch.player_dismissed]
            ) {
              dismissedPlayers[eachMatch.player_dismissed][
                eachMatch.fielder
              ] += 1;
            } else {
              dismissedPlayers[eachMatch.player_dismissed][
                eachMatch.fielder
              ] = 1;
            }
          }
        } else {
          if (eachMatch.dismissal_kind !== "run out") {
            dismissedPlayers[eachMatch.player_dismissed] = {
              [eachMatch.bowler]: 1,
            };
          } else {
          }
          dismissedPlayers[eachMatch.player_dismissed] = {
            [eachMatch.fielder]: 1,
          };
        }
      }
    }

    let maxCount = [];
    let arrayDismissedPlayers = Object.entries(dismissedPlayers);
    for (let eachPlayer of arrayDismissedPlayers) {
      let [dismissedPlayer, bowlers] = eachPlayer;

      let count = Object.values(bowlers);
      let dismissedByCount = Math.max(...count);
      maxCount.push(dismissedByCount);
    }

    let maxCountValue = Math.max(...maxCount);
    let dismissPlayerObject = {};
    for (let eachPlayer of arrayDismissedPlayers) {
      let [dismissedPlayer, bowlers] = eachPlayer;

      let obj = Object.entries(bowlers).filter(([player, count]) => {
        if (count === maxCountValue) {
          return (dismissPlayerObject[dismissedPlayer] = { [player]: count });
        }
      });
    }
    return dismissPlayerObject;
  }
}
csvtojson()
  .fromFile(csvDeliveriesPath)
  .then((deliveriesJson) => {
    csvtojson()
      .fromFile(csvMatchesPath)
      .then((matchesJson) => {
        const playerDismissedResult = playerDismissed(deliveriesJson);
        fs.writeFileSync(
          "src/public/output/8playerDismissed.json",
          JSON.stringify(playerDismissedResult),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
