const csvMatchesPath = "src/data/matches.csv";
const csvDeliveriesPath = "src/data/deliveries.csv";

const csvtojson = require("csvtojson");
const fs = require("fs");

function wonTossWonMatch(matchesJson, deliveriesJson) {
  if (
    matchesJson === undefined ||
    deliveriesJson === undefined ||
    !Array.isArray(matchesJson) ||
    !Array.isArray(deliveriesJson)
  ) {
    return {};
  } else {
    let wonTossWonMatchObject = {};
    for (let eachMatch of matchesJson) {
      if (eachMatch.toss_winner === eachMatch.winner) {
        if (eachMatch.winner in wonTossWonMatchObject) {
          wonTossWonMatchObject[eachMatch.winner] += 1;
        } else {
          wonTossWonMatchObject[eachMatch.winner] = 1;
        }
      }
    }
    return wonTossWonMatchObject;
  }
}
csvtojson()
  .fromFile(csvDeliveriesPath)
  .then((deliveriesJson) => {
    csvtojson()
      .fromFile(csvMatchesPath)
      .then((matchesJson) => {
        const wonTossWonMatchResult = wonTossWonMatch(
          matchesJson,
          deliveriesJson
        );
        fs.writeFileSync(
          "src/public/output/5wonTossWonMatch.json",
          JSON.stringify(wonTossWonMatchResult),
          "utf-8",
          (error) => {
            console.log(error);
          }
        );
      });
  });
